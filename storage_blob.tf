resource "random_string" "random_name" {
    length    = 5
    upper     = false
    lower     = true
    number    = true
    special   = false
}

resource "azurerm_storage_account" "devstgac" {
    name                        = "devstgac${random_string.random_name.result}"
    resource_group_name         = azurerm_resource_group.rg1.name
    location                    = azurerm_resource_group.rg1.location
    account_tier                = var.account_tier
    account_replication_type    = var.account_replication_type
    access_tier                 = var.access_tier
}

resource "azurerm_storage_container" "devstgcon" {
     name = "devstgcon${random_string.random_name.result}"
     storage_account_name = azurerm_storage_account.devstgac.name
     container_access_type = var.access_type
}

resource "azurerm_storage_blob" "devblob" {
     name                      = var.filename != "" ? var.filename : "filename1..txt"
     storage_account_name      = azurerm_storage_account.devstgac.name
     storage_container_name    = azurerm_storage_container.devstgcon.name
     type                      = var.blob_type
     source                    = var.filename != "" ? var.filename : "filename1.txt"
}